Raining Cats and Dogs in Australia tomorrow?!

For this little challenge, we will build different machine learning models using Logistic Regression, SVM, and neural networks to predict the likelihood of rain in Australia tomorrow based on a 9 year historical data.