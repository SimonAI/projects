**Data Science Portfolio**

Repository containing portfolio of data science projects using Jupyter Notebook completed by me in my free time for academic and self-learning purposes.



**Contents**
___________________________________________________________________________________________________________________
**Machine Learning**

•	Bank Loan Prediction: A model to predict whether customers would repay bank loan using various traits such as education, age, and gender. Identified and evaluated the most optimized algorithm(s) using machine learning for this classification problem.

•   Weather Prediction Model: A model to predict the likelihood of rain in Australia tomorrow based on a 9 year historical data using Logistic Regression, SVM, and neural networks.





_____________________________________________________________________________________________________________________





**HOW TO USE:**


1.	Download anaconda. Install Jupyter Notebook from within anaconda.
2.	Set up an environment. An environment contains all packages in one place.
3.	Install the following packages (pandas, seaborn, matplotlib) using anaconda for the Bank Loan Prediction project. Or from the terminal for that specific environment, type 'conda install pandas'.
4.	Open up this notebook and run all.


